<?php get_header(); ?>

<div id="content">
	
	<?php
	if ($post->ID == 2 || $post->ID == 5 || $post->ID == 7) {
		for ($i=1; $i<=4; $i++) {
			?> <div id="l_sidebar"><ul id="l_sidebarwidgeted"> <?php
			dynamic_sidebar($post->post_title." ".$i);
			?> </ul></div> <?php
		}
	}
	else {
	?>
	
	<div id="contentleft">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<h1><?php the_title(); ?></h1>
		<?php the_content(__('Read more'));?><div style="clear:both;"></div>
		
		<?php endwhile; else: ?>
		
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
		
	</div>
		
	<?php } /* include(TEMPLATEPATH."/l_sidebar.php"); ?>
	
	<?php include(TEMPLATEPATH."/r_sidebar.php"); */ ?>
	

</div>

<!-- The main column ends  -->

<?php get_footer(); ?>